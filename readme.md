# Svelte tutorial for MDN - WIP

Svelte tutorial for the [Understanding client-side JavaScript frameworks](https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks) series at Mozilla Developer Network.

## Contents

### 1. Getting started with Svelte: [01-svelte-getting-started](./01-svelte-getting-started/Svelte_getting_started.md)

In this article we'll provide a quick introduction to Svelte. We will see how Svelte works and what sets it apart from the rest of the frameworks and tools we've seen so far. Then we will learn how to setup our development environment, create a sample app, understand the structure of the project, and see how to run the project locally and build it for production.

### 2. Starting our To-Do list app: [02-svelte-starting-our-todo-app](./02-svelte-starting-our-todo-app/Svelte_starting_our_todo_app.md)

In this section we will first have a look at the desired functionality of our app, then we'll create our `Todos.svelte` component and put static markup and styles in place, leaving everything ready to start developing our To-Do list app features. 

Start from this [repl](https://svelte.dev/repl/eb0ffd5facd14815859560684e11d742?version=3.23.2) to follow along.

### 3. Adding dynamic behavior: working with variables and props [03-svelte-adding-dynamic-behavior](./03-svelte-adding-dynamic-behavior/Svelte_adding_dynamic_behavior.md)

In this article we'll be using variables and props to make our app dynamic, allowing us to add and delete todos, and mark them as complete. 

Start from this [repl](https://svelte.dev/repl/48b7a49037364fe98351f2ce123abad2?version=3.23.2) to follow along.

### 4. Componentizing our app: [04-svelte-componentizing-our-app](./04-svelte-componentizing-our-app/Svelte_componentizing_our_app.md)

In this section we will add more features to our add and we'll see how to break our app into manageable components. We will learn how to extract functionality to a new component and we'll see different techniques to let a child component return information to its parent: using a handler passed as a prop, using the `bind` directive to implement two-way data binding, using custom events to implement the props-up events-down communication pattern

Start from this [repl](https://svelte.dev/repl/99b9eb228b404a2f8c8959b22c0a40d3?version=3.23.2) to follow along.

### 5. Svelte advanced concepts: [05-svelte-advanced-concepts](./05-svelte-advanced-concepts/Svelte_advanced_concepts.md)

In this article we complete all the planned features and we'll have a closer look at Svelte reactivity. We'll also take care of some accessibility issues and see some advanced topics like: working with DOM node using the `bind:this={dom_node}` directive, component lifecycle functions, using the `use:action` directive to add functionality to HTML elements and using the `bind:this={component}` to access component methods.

Start from this [repl](https://svelte.dev/repl/76cc90c43a37452e8c7f70521f88b698?version=3.23.2) to follow along.

### 6. Stores & transitions: [06-svelte-stores](./06-svelte-stores/Svelte_stores.md)

In this article we will show another way to handle state management in Svelte — Stores. [Stores](https://svelte.dev/tutorial/writable-stores) are global global data repositories that hold values. Components can subscribe to stores and receive notifications when their values change.

Using stores we will create an `Alert` component that shows notifications on screen, which can receive messages from any component. 

Then we'll see how to develop our own custom store to persist the todo information to [web storage](https://developer.mozilla.org/en-US/docs/Web/API/Web_Storage_API), allowing our todos to persist over page reloads.

We will also had a look at using the Svelte `transition` directive to implement animations on DOM elements.

Start from this [repl](https://svelte.dev/repl/d1fa84a5a4494366b179c87395940039?version=3.24.0) to follow along.

### 7. TypeScript support in Svelte: [07-svelte-typescript-support](./07-svelte-typescript-support/Svelte_typescript_support.md)

In this article will learn how to use TypeScript to develop Svelte applications. First we'll learn what is TypeScript and what benefits it can bring us. Then we'll see how to configure our project to work with TypeScript files. Finally we will go over our app and see what modifications we have to make to fully take advantage of TypeScript features.

> [TypeScript support](https://github.com/sveltejs/svelte-repl/issues/130) is not available from the REPL yet. You can download the content folder and work locally with `npx degit opensas/mdn-svelte-tutorial/07-typescript-support`.

### 8. Deployment, and next Steps: [08-svelte-next-steps](./08-svelte-next-steps/Svelte_next_steps.md)

In this article we'll learn about a couple of zero-fuss options to deploy our app in production and also how to setup a basic pipeline to deploy our app to GitLab on every commit. We will also learn how to configure our application to enable Svelte's TypeScript support. Finally we'll see which resources are available on line to keep learning Svelte.

Start from this [repl](https://svelte.dev/repl/378dd79e0dfe4486a8f10823f3813190?version=3.24.0) to follow along.