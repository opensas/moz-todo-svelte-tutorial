# Svelte tutorial for MDN - WIP

Svelte tutorial for the [Understanding client-side JavaScript frameworks](https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks) series at Mozilla Developer Network.

> Note: this is a draft version

## Plan

### 1. Getting started with Svelte: [01-svelte-getting-started](./01-svelte-getting-started/Svelte_getting_started.md)

status: ready

In this article we'll provide a quick introduction to Svelte. We will see how Svelte works and what sets it apart from the rest of the frameworks and tools we've seen so far. Then we will learn how to setup our development environment, create a sample app, understand the structure of the project, and see how to run the project locally and build it for production.

end result [repl](https://svelte.dev/repl/eb0ffd5facd14815859560684e11d742?version=3.23.2)

### 2. Starting our To-Do list app: [02-svelte-starting-our-todo-app](./02-svelte-starting-our-todo-app/Svelte_starting_our_todo_app.md)

status: ready

Clean-up, Todo.svelte component, static markup and styling

end result [repl](https://svelte.dev/repl/48b7a49037364fe98351f2ce123abad2?version=3.23.2)

### 3. Developing basic functionality: [03-svelte-developing-basic-functionality](./03-svelte-developing-basic-functionality/Svelte_developing_basic_functionality.md)

status: ready

Now that we have a basic understanding of how things works in Svelte, we can start building our To-Do list app.

We want our users to be able to add, edit and delete tasks, and also to mark them as complete. This will be the basic functionality that we'll be developing in this article. 

Features to develop: display, add, delete and toggle todos, show status

end result [repl](https://svelte.dev/repl/99b9eb228b404a2f8c8959b22c0a40d3?version=3.23.2)

### 4. Componentizing our app: [04-svelte-componentizing-our-app](./04-svelte-componentizing-our-app/Svelte_componentizing_our_app.md)

status: ready

In this section we will add more features to our add and we'll see how to break our app into manageable components.

- Filter.svelte - component two way data binding

- Todo.svelte - props up events down

- Todo.svelte - editing todos

- NewTodo.svelte

- TodoStatus.svelte

- MoreActions.svelte

end result [repl](https://svelte.dev/repl/76cc90c43a37452e8c7f70521f88b698?version=3.23.2)

### 5. Svelte advanced concepts: [05-svelte-advanced-concepts](./05-svelte-advanced-concepts/Svelte_advanced_concepts.md)

status: pending review

### 6. Stores & transitions: [06-svelte-stores](./06-svelte-stores/Svelte_stores.md)

status: pending review

Then we'll implement an `Alert` component to have a look at another way to share information among components: stores. We will also give the final touches to our app and see some other Svelte advanced techniques.

- Alert.svelte: stores/alert.js

- Alert.svelte: making dependencies explicit: `$: onMessageChange($alert, ms)`

- transitions: https://svelte.dev/docs#fly

### 7. finished app (so far)

end result [repl](https://svelte.dev/repl/378dd79e0dfe4486a8f10823f3813190?version=3.23.2)


## Online editing alternatives

- [svelte repl](https://svelte.dev/repl/hello-world?version=3.23.2)

- [Scrimba](https://scrimba.com/c/c73wKaHW)

- Codesandbox: [importing branches(https://codesandbox.io/docs/importing)]

- Stackblitz

- Glitch

https://www.gitpod.io/pricing/

https://stackshare.io/gitpod/alternatives

https://www.codeonline.io/pricing.html



# Pending tasks

## good explanation of stores:

https://dev.to/joshnuss/managing-state-in-svelte-29o7


Stores

Not all state belongs in the component tree. Sometimes there are visually disconnected components sharing the same state.

Imagine an app with a logged in user. It would be tedious to pass the user= prop to every component. Many components would have to take the user= prop, just to pass it along because a grand-child or great-grand-child needed it.

This is where using a store makes sense, we can centralize the state of the user into a store. When a component needs user data, it can import it with import {user} from './stores'.




## add note about accessibility features on svelte

https://github.com/sveltejs/svelte/issues/374

https://github.com/sveltejs/svelte/issues/820

description of supported rules

https://github.com/sveltejs/svelte/issues/820#issuecomment-637333918

autofocus

how to disable warnings

see: https://stackoverflow.com/a/61277660/47633

https://github.com/sveltejs/rollup-plugin-svelte#usage

## talk about the repl and git repo

https://svelte.dev/repl/2c06d586d890466e8dc7da6fb111efb7?version=3.22.3

## showcase typescript support

https://github.com/sveltejs/svelte/issues/4518

https://github.com/sveltejs/language-tools/blob/master/docs/preprocessors/typescript.md#troubleshooting--faq

step by step configuration:

https://github.com/sveltejs/language-tools/blob/master/docs/preprocessors/typescript.md#getting-it-to-work-for-your-build

```
npm i -D svelte-preprocess typescript tslib @rollup/plugin-typescript

OK 1. npm i -D svelte-check svelte-preprocess @rollup/plugin-typescript typescript tslib @tsconfig/svelte 

OK 2. package.json: add

  "validate": "svelte-check"

to package.json -> scripts

OK 3. main.js: rename src/main.js to src.main.ts

4. App.svelte: add `<script lang="ts">` to App.svelte

5. rollup.config.js: add 

import sveltePreprocess from 'svelte-preprocess';
import typescript from '@rollup/plugin-typescript';

OK 6. rollup.config.js: replace src/main.js with src/main.ts 

OK 7. rollup.config.js: add `preprocess: sveltePreprocess()`, at the end of svelte plugin config

OK 8. rollup.config.js: add `typescript({ sourceMap: !production })` after commonjs()

OK 9: tsconfig.json:

```
{
  "extends": "@tsconfig/svelte/tsconfig.json",

  "include": ["src/**/*"],
  "exclude": ["node_modules/*", "__sapper__/*", "public/*"],
}
```

OK 10: create file .vscode/extensions.json

{
  "recommendations": ["svelte.svelte-vscode"]
}

---

https://github.com/sveltejs/language-tools/issues/161

https://github.com/sveltejs/language-tools/issues/161#issuecomment-638874686

svelte with tyepscript support template

https://github.com/dummdidumm/template

```
npx degit dummdidumm/template svelte-typescript-app
cd svelte-typescript-app
```


https://github.com/sveltejs/svelte/pull/5101

https://github.com/sveltejs/svelte/issues/4518
https://github.com/sveltejs/svelte/blob/blog_ts/site/content/blog/2020-06-04-svelte-and-typescript.md

---

https://github.com/sveltejs/svelte/blob/blog_ts/site/content/blog/2020-06-04-svelte-and-typescript.md#try-today


---

npx degit orta/template#ts
node scripts/setupTypeScript.js

npx degit orta/template#ts/scripts

https://github.com/sveltejs/svelte/issues/5016#issuecomment-

---
issues to report

1. adding a ts file on a new project -> error

2. Action type -> 

3. Automatic imports above the <script> tag

4. Whats the type of a Svelte component???

5. Export string literal types -- error



---

temporaly remove surge section

[Surge](https://surge.sh/) is also a pretty straightforward option to deploy your front-end app from the command line. Just build your app with `npm run build`, go to the folder you want to publish (in this case the `public`) folder and run `npx surge`.

You'll be prompted to enter an email address and password when you run it, after which the installation process should look like this:

```shell
$ npm run build
src/main.js → public/build/bundle.js...
created public/build/bundle.js in 1.8s

$ cd public

$ npx surge
npx: installed 162 in 12.7s

   Running as xxxx@xxx.com

        project: [...]/mdn-svelte-todo/public/
         domain: mdn-svelte-todo.surge.sh
         upload: [====================] 100% eta: 0.0s (7 files, 139253 bytes)
            CDN: [====================] 100%

             IP: 138.197.235.123

   Success! - Published to mdn-svelte-todo.surge.sh
```

[CHRIS - I TRIED THIS SEVERAL TIMES, BUT IT SEEMS TO GET STUCK ON THE "project" LINE, AND THEN DOESN'T GO ANY FURTHER. I CAN'T GET THIS TO DEPLOY.]
~


--- things to fix:

https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks/Svelte_getting_started#Using_the_Svelte_REPL

The bar above the code lets you create .svelte and .js files and rearrange them. To create a file inside a folder just specify the complete pathname, like this — components/MyComponent.svelte. The folder will be automatically created.

---

Draft tutorial for Svelte to be published ant Mozilla Developer Network Web Docs

the last few weeks we've been working with the people at Mozilla Developer Network to create a series of tutorials about Svelte. The tutorials will be published on the [Client-side Js frameworks](https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks) section, alongside React, Vue and Ember.

We are uploading the draft versions of the chapters to let the Svelte community review it and have a chance to propose changes before getting published.

We have already uploaded the first three chapter ([1](https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks/Svelte_getting_started), [2](https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks/Svelte_todo_list_beginning) and [3](https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks/Svelte_variables_props), and this is the github repository with the rest of the tutorial: [mdn-svelte-tutorial](https://github.com/opensas/mdn-svelte-tutorial) 

We tried our best to produce an introductory, but rather complete, material, showing the pros/and cons of each approach and trying to show best practices. 

Everybody is more than welcome to help us improve it!

---

tweet about it:

The fourth chapter draft version of @MozDevNet Svelte tutorial is online
https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks/Svelte_components
for everyone to help us review it.
We talk about how to structure your apps in components, and the pros and cons of different techniques to share data between parent and child components.


The draft version of the fifth chapter draft version of @MozDevNet Svelte tutorial is online
https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks/Svelte_reactivity_lifecycle_accessibility!!!
for everyone to help us review it.

- reactivity gotchas and how to avoid them
- binding to DOM nodes
- onMount() function, 
- use:action 
- binding to components.


We talk about how to structure your apps in components, and the pros and cons of different techniques to share data between parent and child components.

---


The 6th chapter of @MozDevNet Svelte tutorial (draft) is online
https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks/Svelte_stores
We talk about stores, Svelte powerful but super simple alternative to redux and vuex.
- what are Svelte stores?
- subscribe and unsubscribe from stores
(1/4)

(cont...)
- reactive stores with the auto-subscription syntax ($store)
- the store contract and how to implement it from scratch
- creating customs stores
- we create our own custom persistable store to save our app state to web storage 
(2/3)

(cont...)
We also implement a notification component to show how to use stores as a centralized data repository shared among many unrelated components.

The end result is on this repl: https://svelte.dev/repl/378dd79e0dfe4486a8f10823f3813190?version=3.23.2
(3/3)

You can code with us online using this repl: https://svelte.dev/repl/d1fa84a5a4494366b179c87395940039?version=3.23.2

And here you can download the source code: https://github.com/opensas/mdn-svelte-tutorial/tree/master/06-stores

cc @SvelteSociety @MozDevNet 

special kudos to @chrisdavidmills who is working really hard to get this material published as soon as possible!!!


----

We are at full speed publishing the 7th chapter of @MozDevNet Svelte tutorial (draft)
https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks/Svelte_TypeScript
We take a deep dive into the brand new TypeScript support in Svelte:
- gentle introduction to @typescript, pros and cons
(1/3)

- adding TypeScript support to Svelte apps
- improve the developer experience with the official Svelte for @code extension
- validating your code from the command line with svelte-check
- intro to ts generics and how ot use them to bullet proof our custom stores 
(2/3)

And finally we show you how to port our whole app, step by step, to take advantage of TypeScript features

Here you can download the JavaScript app to port it to ts with us: https://github.com/opensas/mdn-svelte-tutorial/tree/master/07-typescript-support

And this is the TypeScript version of our app: https://github.com/opensas/mdn-svelte-tutorial/tree/master/08-next-steps
(3/3)

Great applause to our svelte community heroes: @orta, dummdidumm, jasonlyu123, @UnwrittenFun, @halfnelson_au, @octref, @swyx, @evilpingwin, and many others that made this possible, and keep improving it everyday! (and that answered all my questions in record time!)

cc @Sveltejs, @SvelteSociety @MozDevNet 

---



Hi!
I'm Sebastian Scarano, and as you know I've been working with @chrisdavidmills from MDN to create a series of Svelte Tutorials.
We tried to contact Rich Harris at richard.a.harris@gmail.com, like it appears on his github profile, but it seems like he is not checking that account anymore.
Do you know how can MDN people get in contact with him?
We'd like to invite svelte devs to have a look at the tutorial before officially publishing it. We'd like to make sure we are representing Svelte in the best possible light, and not saying anything that is considered bad practice.
Thanks a lot

PS: I guess you know, but in the last few weeks most of the MDN team got fired from Mozilla. This is terrible news. I hope we can take the opportunity to show our support for the work carried out by MDN.
---